const request = require('request');
const fs = require('fs');

function requestPage(localFilePath, apiurl, pageNo) {
  if (pageNo == undefined) {
    pageNo = 1;
  }
  if (pageNo == 1) {
    console.log("Clearing housing.json");
    fs.truncateSync(localFilePath, 0);
  }
  
  console.log("Making a request to the API...");
  request(apiurl + pageNo, (err, response, body) => {
    if (err)
      return console.error(err);

    let data = JSON.parse(body).data;
    console.log("data.nextPage",data.nextPage);
    console.log("pageNo", pageNo);
    try {
      appendToFile(localFilePath, data);
    } catch (e) {
      console.error(e);
    }

    if (data.nextPage == true) {
      pageNo++;
      requestPage(localFilePath, apiurl, pageNo);
    }
    else {
      console.log(`Successfully downloaded the housing board data to ${localFilePath}`)
      return;
    }
  });
}

function appendToFile(localFilePath, data) {
  // Write to file
  console.log("Writing API Response to file...");
  // data is passed parsed
  let writeData = JSON.stringify(data.items);
  try {
    fs.appendFileSync(localFilePath, writeData);
  } catch (err) {
    console.error(err);
    return false;
  }

  console.log(`Successfully written to file ${localFilePath}`);
}

//Go
let apiurl = "https://api.pro.mabibase.com/housing/?field=item&server=triona&sort=price&order=asc&limit=50&page=";
let localFilePath = './housing.json';
requestPage(localFilePath, apiurl);