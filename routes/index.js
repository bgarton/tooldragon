var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  // Cookies that have not been signed
  //console.log('Cookies: ', req.cookies)
  if (!req.cookies.transformToggle) {
    res.cookie("transformToggle", "yes", {
      expires: new Date(Date.now() + 900000),
      httpOnly: false,
      secure: process.env.NODE_ENV === "dev" ? false : true,
    });
  }

  res.render("index", {
    title: "Tooldragon",
    desc:
      "A brand new home of tools and useful links for Mabinogi Pro. Turn off the transformation timer via the toggle on the Erinn Clock above.",
    notiToggle: req.cookies.transformToggle
  });
});

module.exports = router;
