var express = require('express');
var router = express.Router();
var path = require('path')
var bodyParser = require('body-parser');

/* GET home page. */
router.get('/', bodyParser.json(), function (req, res) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  if (!req.body) return res.sendStatus(400);
  var fs = require('fs');
  var data = fs.readFileSync(path.join(__dirname, '../nonpublic/housing.json'), { encoding: 'utf8', flag: 'r' });
  data = JSON.parse(data);
  res.json(data);
});
module.exports = router;
