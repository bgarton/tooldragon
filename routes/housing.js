var express = require('express');
var router = express.Router();
var path = require('path');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  if (!req.cookies.transformToggle) {
    res.cookie("transformToggle", "yes", {
      expires: new Date(Date.now() + 900000),
      httpOnly: false,
      secure: process.env.NODE_ENV === "dev" ? false : true,
    });
  }
  res.render("housing", {
    title: "Housing",
    notiToggle: req.cookies.transformToggle
  });
});
module.exports = router;
