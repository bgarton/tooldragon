const request = require('request');
const fs = require('fs');
const path = require('path');
const https = require('https');

function writeJsonFile(data, fpath, overwrite = false) {
  console.log("Writing data");
  try {
    if (overwrite) {
      // appending additional data? ...
      let currData = readJsonFile(fpath);
      currData.items.push(data);
      data = currData;
    }

    fs.writeFileSync(fpath, JSON.stringify(data));
    return true;
  } catch (err) {
    console.error(err)
    return false;
  }
}

function readJsonFile(fpath) {
  try {
    return JSON.parse(fs.readFileSync(fpath, 'utf8'))
  } catch (err) {
    console.error(err)
    return false
  }
}

// Request housing board data
/* async function requestData(apiurl, page) {
  return fetch(apiurl + page)
    .then(function (response) {
      return response.json()
    }).then(function (json) {
      console.log('Retreived api data', json)
    }).catch(function (ex) {
      console.log('parsing api data failed', ex)
    })
} */

async function requestData(apiurl) {
  return new Promise((resolve, reject) => {
    console.log("Making a request to the API...");

    https.get(apiurl, (res) => {
      let data = '';

      // A part of the data has been recieved.
      res.on('data', (chunk) => {
        data += chunk;
      });

      // The whole response has been received.
      res.on('end', () => {
        //console.log(JSON.parse(data));
        resolve(JSON.parse(data).data);
      });

    }).on("error", (err) => {
      reject(err);
    });
  });
}

async function __main__() {
  let apiurl = "https://api.pro.mabibase.com/housing/?field=item&server=triona&sort=price&order=asc&limit=50&page=";
  let fpath = path.join(__dirname, 'housing.json');
  console.log(fpath, "fpath");

  // If page number <= 1 then create housing.json with empty object then proceed?
/*   if (page <= 1) {
    writeJsonFile("{}", fpath);
    page = 1; // We need the first page data response
  } */
  let page = 0;
  // Read from json file / make our json
/* let json = readJsonFile(fpath); */
  let json = [];

  // Request our page
  let remotejson = await requestData(apiurl+page); // returns (res).data.items, (res).data.nextPage
  console.log(remotejson.items[0], page, remotejson.nextPage, "remotejson items len,page,remotejson.nextPage");

  // We want to keep calling requestData() until data.nextPage == false, then write it
  while (remotejson.nextPage == true) {
    json.push(... remotejson.items);
    // Get next page results
    page++;
    remotejson = await requestData(apiurl+page);
    console.log(remotejson.items[0], page, remotejson.nextPage, "remotejson items len,page,remotejson.nextPage");
  }
  // remotejson.nextPage is finally false
  json.push(... remotejson.items);

  // Write to file
  console.log(json.length, "json length");
  writeJsonFile(json, fpath);
}

__main__();