// Functionality
let warned = false;
var tid = setTimeout(loop, 10);
function loop() {
  //add the offset (in milliseconds) to UTC to get server time
  let serverTime = getServerTimeMillis();
  let beepTime = { hour: 5, minute: 40 };
  let churchPtj = { from: { hour: 12, minute: 0 }, to: { hour: 19, minute: 59} };
  let healerPtj = { from: { hour: 6, minute: 0 }, to: { hour: 14, minute: 59} };

  //game time
  let erinnTime = serverTimeToErinnTime(serverTime);
  let erinnHour = erinnTime.hour;
  let erinnMinute = erinnTime.minute;

  //should we beep?
  if (Number(erinnHour) == beepTime.hour && Number(erinnMinute) == beepTime.minute && !warned && getCookie('transformToggle')=="yes") {
    // make noise
    var sound = new Howl({
      src: ['/sounds/enharpment.ogg'],
      autoplay: true,
      loop: false
    });

    warned = true;
    sound.play();
  }
  if (Number(erinnHour) != beepTime.hour && Number(erinnMinute) != beepTime.minute) {
    warned = false;
  }

  //change clock onscreen
  alterDisplay(erinnHour, erinnMinute, churchPtj, healerPtj);

  tid = setTimeout(loop, 1500); // repeat myself
}
function abortTimer() { // to be called to stop the timer
  clearTimeout(tid);
}
function alterDisplay(erinnHour, erinnMinute, churchPtj, healerPtj) {
  document.getElementById("eclock").innerHTML = `${erinnHour}:${erinnMinute}`;

  if (Number(erinnHour) == 5 && (Number(erinnMinute) >= 0 && Number(erinnMinute) <= 59)) {
    document.getElementById("eclock").innerHTML += `<div class='pt-2'><p class='is-size-4 has-text-warning transform'>Transform!</p></div>`;
  }
}

// Cookies
function setCookie(name, value, days) {
  var expires = "";
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
}


// Minimize button
document.addEventListener('DOMContentLoaded', function () {
  // On transformation toggle click
  const checkbox = document.getElementById('transformSwitch')

  checkbox.addEventListener("change", (event) => {
    (event.currentTarget.checked) ? setCookie('transformToggle', 'yes') : setCookie('transformToggle', 'no');
  });

  let cardToggles = document.getElementsByClassName('card-toggle');
  for (let i = 0; i < cardToggles.length; i++) {
    cardToggles[i].addEventListener('click', e => {
      e.currentTarget.childNodes[0].childNodes[0].classList.toggle('fa-angle-down');
      e.currentTarget.childNodes[0].childNodes[0].classList.toggle('fa-angle-up');
      e.currentTarget.parentElement.parentElement.childNodes[1].classList.toggle('is-hidden');
    });
  }
});