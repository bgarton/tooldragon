document.addEventListener('DOMContentLoaded', function () {

  function alterDisplayPtj(key, value, erinnHour) {
    let child = document.createElement('p'); // part of output

    if (Number(erinnHour) >= value.from.hour && Number(erinnHour) <= value.to.hour) {
      // display ptj element
      title = `${key} PTJ`;
      child.classList.add("has-text-info");

      if (Number(erinnHour) >= value.to.hour - 3) {
        // make red?
      }

      document.getElementById("ptj").appendChild(child);
    } // else do nothing
  }

  var tid = setTimeout(ptjLoop, 10);

  function ptjLoop() {
    document.getElementById("ptj").innerHTML = "";
    const ptjs = {
      church: {
        from: { hour: 12, minute: 0 },
        to: { hour: 19, minute: 59 }
      },
      healer: {
        from: { hour: 6, minute: 0 },
        to: { hour: 14, minute: 59 }
      }
    }

    //game time
    let serverTime = getServerTimeMillis();
    let erinnTime = serverTimeToErinnTime(serverTime);
    let erinnHour = erinnTime.hour;
    let erinnMinute = erinnTime.minute;
    let title = "None";

    // Clear ptj list
    document.getElementById("ptj").innerHTML = "None";

    for (const [key, value] of Object.entries(ptjs)) {
      //console.log(`${key}: ${JSON.stringify(value)}`);
      alterDisplayPtj(key, value, erinnHour);
    }

    tid = setTimeout(ptjLoop, 1500); // repeat myself
  }
});