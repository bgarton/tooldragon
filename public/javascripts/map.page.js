document.addEventListener('DOMContentLoaded', function () {
  var element = document.querySelector('#umap');
  var map = L.map('umap', {
    crs: L.CRS.Simple
  });

  var bounds = [[0, 0], [1000, 1000]];
  var image = L.imageOverlay('/images/umap.jpg', bounds).addTo(map);
  map.fitBounds(bounds);
});