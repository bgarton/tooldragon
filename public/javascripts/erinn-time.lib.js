// Setup
var TIME_PER_ERINN_MINUTE = 1500; // 1.5 s
var TIME_PER_ERINN_HOUR = TIME_PER_ERINN_MINUTE * 60; // 1 min 30 s
var TIME_PER_ERINN_DAY = TIME_PER_ERINN_HOUR * 24; // 36 min

const getServerTimeMillis = (date) => {
  const hourOffset = moment().tz("America/New_York").isDST() ? -3 : -4;
  const serverOffset = hourOffset * 60 * 60 * 1000 - 60 * 1000 * 60; //GMT - 7 or -6, New York
  const serverDelayOffset = 8000; // Seems to be a few seconds delay on server side
  return (date || new Date()).getTime() + serverDelayOffset + serverOffset;
};

const serverTimeToErinnTime = function () {
  let serverTime = getServerTimeMillis();
  let hour = Math.floor(serverTime / TIME_PER_ERINN_HOUR) % 24;
  let minute = Math.floor(serverTime / TIME_PER_ERINN_MINUTE) % 60
  hour = (hour <= 9) ? `0${hour}` : hour;
  minute = (minute <= 9) ? `0${minute}` : minute;

  return {
    'hour': hour,
    'minute': minute
  };
}