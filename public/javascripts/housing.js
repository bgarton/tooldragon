function formatMoney(number, decPlaces, decSep, thouSep) {
  decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
    decSep = typeof decSep === "undefined" ? "." : decSep;
  thouSep = typeof thouSep === "undefined" ? "," : thouSep;
  var sign = number < 0 ? "-" : "";
  var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
  var j = (j = i.length) > 3 ? j % 3 : 0;

  return sign +
    (j ? i.substr(0, j) + thouSep : "") +
    i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
    (decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
}

document.addEventListener('DOMContentLoaded', function () {
  var btn = document.getElementById('generateHousing');

  function generateHousing() {

    function generateIconImg(ele, id, color1, color2, color3) {
      // id:63024 = dye amp
      if (id == undefined || id == null || id == 0)
        id = 63024;
      let iconDbUrl = `https://api.pro.mabibase.com/icon/item/${id}?c1=${color1}`;
      if (typeof color2 !== undefined)
        iconDbUrl += `&c2=${color2}`;
      if (typeof color3 !== undefined)
        iconDbUrl += `&c3=${color3}`;
      ele.classList.add('lazyload');
      ele.setAttribute('data-src', iconDbUrl);
      return ele;
    }

    // Loading bar
    document.getElementById('housing-search').classList.add('is-hidden');
    document.getElementById('generated-time').classList.add('is-hidden');
    document.getElementById('housing-table').innerHTML = "<div class=\"has-text-centered mt-4\"><i class=\"fas fa-spinner fa-pulse fa-4x\"></i></div>";

    // Update button text
    document.getElementById('generateHousing').childNodes[1].textContent = "Refresh";

    // request
    var request = new XMLHttpRequest();
    //var protocol = (location.protocol !== 'https:') ? 'http' : 'https';
    request.open('GET', '/housing/data.json', true); // public dir

    request.onload = function () {
      if (this.status >= 200 && this.status < 400) {

        // Success!
        var data = JSON.parse(this.response);
        //console.log(data);
        let tr = null, td = null, th = null, p = null, icon = null, iconDiv=null;
        let itemDbUrl = "https://pro.mabibase.com/item/";
        

        // Update screen function
        let date = new Date().toLocaleString();
        document.getElementById('housing-table').innerHTML = "";

        var table = document.createElement("table");
        table.classList.add('table', 'is-striped', 'is-hoverable');
        var thead = document.createElement("thead");
        thead.classList.add('has-background-warning');
        var tfoot = document.createElement("tfoot");
        var tbody = document.createElement("tbody");

        // Table headers
        let headers = ['', 'Name', 'Price', 'Seller'];
        headers.forEach(function (value, index) {
          th = document.createElement('th');
          th.classList.add('has-text-grey-dark');
          th.innerHTML = value;
          thead.appendChild(th);
        });

        // Table body
        data.forEach(item => {
          // Table row <tr>
          tr = document.createElement('tr');

          // Table items/cells

          // Icon / image
          th = document.createElement('th');
          th.classList.add('has-text-centered', 'vcenter');
          
          iconDiv = document.createElement('div');
          iconDiv.style.display = 'inline-block';
          iconDiv.classList.add('item-icon','tooltip');
          
          icon = new Image();
          iconBig = new Image();
          icon.alt = item.name;

          let imgIcon = generateIconImg(icon, item.id, ...item.colors);
          imgIcon.classList.add('item-icon');
          let imgIconBig = generateIconImg(iconBig, item.id, ...item.colors);
          let span = document.createElement('span');
          span.innerHTML = imgIconBig.outerHTML; // put big image into tooltiptext span
          span.classList.add("tooltiptext");

          if (item.id == 63030 || item.id == 63255 || item.id == 100221) {
            let color = "#" + item.colors[0].substr(2);
            let colorTxt = document.createElement("span");
            th.style.backgroundColor = color;
            colorTxt.textContent = color;
            span.appendChild(colorTxt);
            span.style.backgroundColor = color;
            colorTxt.style.mixBlendMode = "exclusion";
          }

          iconDiv.appendChild(imgIcon); // put small img into icon div
          iconDiv.appendChild(span); // put span into icon div

          th.appendChild(iconDiv);
          tr.appendChild(th)
          td = document.createElement('td');

          // Name
          let link = document.createElement('a');
          link.setAttribute('href', itemDbUrl + item.id);
          link.innerHTML = item.name;
          link.setAttribute('target', '_blank');
          //link.classList.add('has-text-white-ter');
          td.classList.add('vcenter');
          td.appendChild(link);
          tr.appendChild(td)

          td = document.createElement('td');
          td.classList.add('price', 'vcenter');
          td.innerHTML = (item.price).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
          tr.appendChild(td)

          td = document.createElement('td');
          td.innerHTML = item.seller;
          td.classList.add('vcenter');
          tr.appendChild(td)

          // Append our row to the table
          tbody.appendChild(tr);
        });

        // Update the table with our elements
        table.appendChild(thead);
        table.appendChild(tfoot);
        table.appendChild(tbody);

        

        // Finally update the screen
        document.getElementById('generated-time').innerHTML = `Generated at ${date}`;
        document.getElementById('generated-time').classList.remove('is-hidden');
        document.getElementById('housing-table').appendChild(table);
        document.getElementById('housing-search').classList.remove('is-hidden');
      } else {
        // We reached our target server, but it returned an error
        console.error(this);
      }
    };
    request.onerror = function () {
      // There was a connection error of some sort
      console.error("Connection error!");
    };

    request.send();
  }

  // Attach event to button
  btn.addEventListener('click', function (e) { generateHousing(); })

  // Generate it on load?
  generateHousing();

  // Housing search functionality
  /* This should be run when a character is entered into the input */
  function housingSearch() {
    // Select all of the TR dom elements
    let trs = document.querySelectorAll('#housing-table table tbody tr');
    let input = document.getElementById("housing-search").value.toLowerCase();

    trs.forEach(ele => {
      if (!ele.querySelector('td a').textContent.toLowerCase().includes(input)) {
        ele.classList.add('is-hidden');
      } else {
        ele.classList.remove('is-hidden');
      }
    })
  }
  // Attach event to button
  document.getElementById("housing-search").addEventListener('input', housingSearch);
});