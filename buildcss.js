var sass = require("node-sass");
var fs = require("fs");
sass.render({file: "scss/main.scss"}, (err, results) => {
  if (err) { console.log(err); }
  fs.writeFile("public/stylesheets/main.css", results.css, err => console.log);
  console.log('scss -> css complete!');
});