## Installation tips
- Using PM2
- Express with Pug

## IMPORTANT 04/10/21
1. `sudo apt-get install build-essential checkinstall`

Important steps:
1. Login 
2. (Optional) Firewall: `sudo ufw allow 'Nginx Full'`  
3. (Optional) Install Node: 
    1. `curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash`  
    2. `sudo apt-get install -y nodejs`  
5. (Optional) Wordops setup: 
    1. `wo site create tooldragon.xyz --proxy=127.0.0.1:3000`  
    2. `sudo chown $USER:www-data /var/www/tooldragon.xyz/htdocs`  
6. `wo site update tooldragon.xyz -le` to issue an SSL certificate.  
---
- .env is necessary
- Make sure you have an SSL cert for the node site otherwise nginx will redirect to another domain.
- Cron job for housing: `*/15 * * * * cd /var/www/tooldragon.xyz/nodejs/nonpublic && /usr/bin/node housing.dl.js >/dev/null 2>&1`
- Global PM2 install: `sudo npm install pm2 -g`
